build-job:
  stage: build
  script:
    - echo "Hello, First time login!"

test-job1:
  stage: test
  script:
    - echo "This job tests something"

test-job2:
  stage: test
  script:
    - echo "This job tests something, but takes more than time test-job1."

deploy-prod:
  stage: deploy
  script:
    - echo "This job deploys something from the_COMMIT_BRANCH branch."
  environment: production
